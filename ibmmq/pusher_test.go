package ibmmq

import (
	"gitlab.com/kasi-labs/kasi-go-queue/message"
	"testing"
)

func BenchmarkPusher_Push(b *testing.B) {
	config := NewConfig("../env.json")
	pusher, err := NewPusher(config)
	if err != nil {
		b.Error(err)
	}
	for i := 0; i < b.N; i++ {
		pusher.Push(message.NewMessage("", []byte("abc")))
	}

	pusher.Close()
}
