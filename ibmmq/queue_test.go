package ibmmq

import (
	"gitlab.com/kasi-labs/kasi-go-queue/queue"
	"gitlab.com/kasi-labs/kasi-go-queue/queue/unit_test"
	"testing"
)

var newFactory = func(config Config) *queue.NewFactory {
	return &queue.NewFactory{
		NewPopper: func() (queue.Popper, error) { return NewPopper(config) },
		NewPusher: func() (queue.Pusher, error) { return NewPusher(config) },
	}
}

// Test typical Queue push/pop
func TestIbmq_PushPop(t *testing.T) {
	config := NewConfig("../env.json")
	unit_test.TestTropicalPushPop(t, newFactory(config))
}
